import * as http from 'http';
import * as Koa from 'koa';
import * as koa_views from 'koa-views';
import * as koa_json from 'koa-json';
import * as bodyParser from 'koa-bodyparser';
import * as logger from 'koa-logger';
import * as serve from 'koa-static';
import * as onerror from 'koa-onerror';
import * as session from "koa-session2";
import * as config from "./config";
import index from './routes/index';
import TaskManager from './routes/api/TaskManager';
import { Resolve, Reject } from "./Public_Proc"
/*
import api from './routes/api';
import * as db from "./DatabaseHandle";
import * as ExtendMethod from "./ExtendMethod"
import { Resolve, Reject } from "./Public_Proc"
import * as WechatAPI from "./WechatAPI"
// */
var app = new Koa();

onerror(app);

//app.use(bodyParser());

app.use(serve(__dirname + '/../public'));

app.use(koa_views(__dirname + '/../views', {
	extension: 'pug'
}));

let cookie = {
	maxAge: 24 * 60 * 60 * 1000,//cookie有效时长(单位:毫秒)
	expires: '',//cookie失效时间
	path: '/',//写cookie所在的路径
	domain: '',//写cookie所在的域名
	httpOnly: false,//是否只用于http请求中获取
	overwrite: true,//是否允许重写
	secure: '',
	sameSite: '',
	signed: '',
}
app.use(session({
	key: "SESSIONID",
	httpOnly: false,
	cookie: cookie,
	overwrite: true,
	maxAge: 24 * 60 * 60 * 1000,//session有效时长(单位:毫秒)
}));
/*
app.use((ctx: any, next: () => Promise<any>) => {
	if (ctx.session.token === undefined) ctx.session.token=new token();
	//console.log(ctx.session);
	next();
});
// */
app.use(async (ctx: Koa.Context, next: () => Promise<any>) => {
	console.log(`${Date.now()} ${ctx.request.method} ${ctx.request.url}`);
	await next();
});

// data parse
app.use(async (ctx: Koa.Context, next: () => Promise<any>) => {
	if (ctx.method != 'POST' || ctx.path == "/api/FileManager/UploadFile") {
		await next();
		return;
	}
	let promise = new Promise<any>((resolve: Resolve<any>, reject: Reject) => {
		let buffer = '';
		ctx.req.setEncoding('utf8');
		ctx.req.on('data', (chunk: string) => {
			buffer += chunk;
		});
		ctx.req.on('error', (err: Error) => {
			reject(err);
		});

		ctx.req.on('end', () => {
			resolve(buffer);
		});
	});
	try {
		await promise.then((result: any) => {
			ctx.request.body = result;
		});
	} catch (exception) {
		console.error(exception);
	}
	await next();
});
// */

app.use(index.routes());
app.use(index.allowedMethods());
app.use(TaskManager.routes());
app.use(TaskManager.allowedMethods());
app.listen(config.Port);