import * as os from 'os';
export type Resolve<T> = (value?: T | PromiseLike<T>) => void;
export type Reject = (reason?: any) => void;

export function stringIsEmpty(str: string): boolean {
	return (str === null || str === undefined || str === '' || !str);
}
export function IsWindowsOS(): boolean {
	return (os.platform().toUpperCase() == "WIN32" || os.platform().toUpperCase() == "WIN64" || os.platform().toUpperCase() == "WINDOWS");
}

export function IsLinuxOS(): boolean {
	return (os.platform().toUpperCase().indexOf("LINUX") >= 0);
}