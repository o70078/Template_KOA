import * as Router from 'koa-router';
import * as os from 'os';
import * as config from "../config";
const router = new Router();
router.prefix('/');

router.use(async (ctx: Router.IRouterContext, next: () => Promise<any>) => {
	await next();
});

router.get('/', async (ctx: Router.IRouterContext, next: () => Promise<any>) => {
	await ctx.render('index', null);
});

router.get('/Explorer', async (ctx: Router.IRouterContext, next: () => Promise<any>) => {
	await ctx.render('Explorer', null);
});
export default router;