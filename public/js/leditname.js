//名称编辑器组件
Vue.component("editname", {
	props: {
		'value': {
			type: String,
			required: true //必传的
			//default: "默认值" //必传项没有这个
		}
	},
	template: "<div v-bind:style='[style_back]' v-show='VISUAL'>" +
		"	<div v-bind:style='[style_mask]'></div>" +
		"	<div v-bind:style='[style_window]'>" +
		"		<div v-bind:style='[style_title]'>{{Title}}</div>" +
		"		<div v-bind:style='[style_notice]'>{{Notice}}</div>" +
		"		<input style='margin:0px 10px 0px 10px;' type='text' @keyup.13='ok' :value='value' @input=\"$emit('input', $event.target.value)\"></input>" +
		"		<div v-bind:style='[style_buttsdiv]'>" +
		"			<Button id='ok' v-bind:style='[style_okbutt,style_butt]' v-on:click='butt_ok_click'>确认</Button>" +
		"			<Button id='cancel' v-bind:style='[style_cancelbutt,style_butt]' v-on:click='butt_cancel_click'>取消</Button>" +
		"		</div>" +
		"	</div>" +
		"</div>",
	data() {
		return {
			//表示界面是否可见
			VISUAL: false,
			//回调方法
			CALLBACK: null,
			//提示字符
			Notice: "请输入文件夹名称:",
			//窗口标题
			Title: "新建文件夹",

			//确认按钮
			style_okbutt: {
				background: '#00F000',
			},
			//取消按钮
			style_cancelbutt: {},
			style_butt: {
				'user-select': 'none',
				border: '1px solid #ffffff',
				width: "30%"
			},
			//
			style_buttsdiv: {
				height: "30px",
				margin: '10px 20px 0px 20px',
				display: 'flex',
				//'justify-content': 'flex-end',
				'justify-content': 'space-between',
				'flex-direction': 'row',
			},
			//提示语
			style_notice: {
				display: 'flex',
				'flex-direction': 'row',
				'align-items': 'center',
				padding: '0px 20px 0px 10px',
				color: '#000',
				'user-select': 'none',
				'font-size': '25px',
				height: '30%',
			},
			//标签
			style_title: {
				display: 'flex',
				'padding-left': '5px',
				background: 'linear-gradient(to bottom, #454545, #555555)',
				'user-select': 'none',
				height: '30px',
				color: '#FFF',
				'align-items': 'center',
			},
			//窗口
			style_window: {
				'background-color': '#FFF',
				display: 'flex',
				'flex-direction': 'column',
				width: '350px',
				height: '200px',
				'z-index': 101,
				'border-radius': '5px 5px 5px 5px',
				border: '3px solid #555555'
			},
			//透明罩
			style_mask: {
				position: 'absolute',
				left: 0,
				top: 0,
				height: '100%',
				width: '100%',
				color: '#000',
				'z-index': 100,
				filter: 'alpha(opacity=50)',
				'-moz-opacity': 0.5,
				opacity: 0.5,
				'background-color': '#000'
			},
			//背景
			style_back: {
				position: 'absolute',
				left: 0,
				top: 0,
				height: '100%',
				width: '100%',
				display: 'flex',
				'align-items': 'center',
				'justify-content': 'center',
			}
		};
	},
	// */
	methods: {
		butt_ok_click() {
			this.$emit('ok_click', this);
			if (CALLBACK != null) CALLBACK(this, 0);
			this.CloseDialog();
		},
		butt_cancel_click() {
			this.$emit('cancel_click', this);
			if (CALLBACK != null) CALLBACK(this, -2);
			this.CloseDialog();
		},
		/**
		 * 显示界面
		 * @param {string} Title 标题内容
		 * @param {string} Notice 提示语句
		 */
		ShowDialog(callback) {
			CALLBACK = callback;
			this.VISUAL = true;
		},
		CloseDialog() {
			CALLBACK = null;
			this.VISUAL = false;
		},

	},
});