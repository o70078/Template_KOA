/**
 * 类型判断
 * @type {Object}
 */
let UtilType = {
	isPrototype: function (data) {
		return Object.prototype.toString.call(data).toLowerCase();
	},

	isJSON: function (data) {
		return this.isPrototype(data) === '[object object]';
	},

	isFunction: function (data) {
		return this.isPrototype(data) === '[object function]';
	}
}
