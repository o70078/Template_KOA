# 开发与运行环境(**相关工具用更高版本也可以!**):
nodejs(8.9.3)

typescrtipt(2.7.2)

npm(5.5.1)

vscode(1.21.0)

----
# 主要第三方依赖:
### KOA框架
协议:MIT
官网:https://koa.bootcss.com/

### MySQL
协议:MIT

### TypeORM
协议:MIT
官网:http://typeorm.io/

### PUG
协议:MIT
官网:https://pug.bootcss.com/

----
# 开始:
1. 首先需要安装上面 ***开发与运行环境*** 所列出的工具,它们用于开发与运行这个工程
2. 其次需要安装第三方依赖,在工程目录执行
```
npm install
```
即可,npm会自动安装相关依赖.
3. 安装好环境以及依赖库后,执行tsc可将ts代码编译为js代码.
4. 执行这个命令可以把程序运行起来:
```
npm run start
```
如果在VSCode里,可以按F5,一键编译运行.也支持远程挂载调试.
**如果要远程挂载调试需要修改launch.json里的remoteRoot路径.**